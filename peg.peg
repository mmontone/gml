# Hierarchical syntax
Grammar     <- _ Definition+ EndOfFile
Definition  <- Identifier LEFTARROW Expression

Expression  <- Sequence (SLASH Sequence)*
Sequence    <- Prefix*
Prefix      <- (AND / NOT)? Suffix
Suffix      <- Primary (QUESTION / STAR / PLUS)?
Primary     <- Identifier !LEFTARROW
            /  OPEN Expression CLOSE
            /  Literal / Class / DOT

# Lexical syntax
Identifier  <- IdentStart IdentCont* _
IdentStart  <- [a-zA-Z_]
IdentCont   <- IdentStart / [0-9]

Literal     <- ['] (!['] Char)* ['] _
            /  ["] (!["] Char)* ["] _
Class       <- '[' (!']' Range)* ']' _
Range       <- Char '-' Char / Char
Char        <- '\\' [nrt'"\[\]\\]
            /  '\\' 'u' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' 'U' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' [0-3][0-7][0-7]
            /  '\\' [0-7][0-7]?
            /  !'\\' .

LEFTARROW   <- '<-' _
SLASH       <- '/' _
AND         <- '&' _
NOT         <- '!' _
QUESTION    <- '?' _
STAR        <- '*' _
PLUS        <- '+' _
OPEN        <- '(' _
CLOSE       <- ')' _
DOT         <- '.' _

_           <- (Space / Comment)*
Comment     <- '#' (!EndOfLine .)* (EndOfLine/EndOfFile)
Space       <- ' ' / '\t' / EndOfLine
EndOfLine   <- '\r\n' / '\n' / '\r'
EndOfFile   <- !.
