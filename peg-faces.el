;; Emacs package that applies faces to a buffer text using PEGs

(require 'peg)

(defmacro peg-make-string-parser (rules)
  `(lambda (string &optional noerror)
     (peg-parse-string ,rules string noerror)))

(defmacro peg-def-parser (name &rest rules)
  `(put ',name 'peg-parser
        (peg-make-string-parser ,rules)))

(defun peg-find-parser (name)
  (get name 'peg-parser))

(defun peg-parser-parse-string (parser-name string &optional errorp)
  (funcall (peg-find-parser parser-name) string errorp))

(peg-add-method translate peg-face (exp face)
  `(and (region ,(peg-translate-exp exp))
        `(to from -- (list ,face to from))))

(peg-add-method normalize peg-face (exp face)
  (let* ((action `(to from -- (list ,face to from)))
         (stack-action (peg-normalize `(stack-action ,action))))
    (peg-normalize `(and (region ,exp)
                         ,stack-action))))

(defmacro def-peg-faces-table (name &rest cases)
  `(defun ,name (key)
     (ecase key
       ,@cases)))

(defun get-string-from-file (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

(defun peg-faces-propertize (start end &optional loudly)

  "Propertize using PEG"
  
  (let ((regions
         (rest
          (peg-parser-parse-string
           peg-faces
           (buffer-substring-no-properties 1 (buffer-size))))))
    (dolist (region regions)
      (when
          ;;(and (>= (second region) start)
          ;; (<= (third region) end))
          (or (and (>= (second region) start)
                   (<= (second region) end))
              (and (>= (third region) start)
                   (<= (third region) end)))
        (put-text-property
         (second region)
         (third region)
         'face
         (funcall peg-faces-table (first region)))))))

(def-peg-faces-table peg-default-faces-table
  (:var 'font-lock-variable-name-face)
  (:builtin 'font-lock-builtin-face)
  (:keyword 'font-lock-keyword-face)
  (:type 'font-lock-type-face)
  (:constant 'font-lock-constant-face)
  (:comment 'font-lock-comment-face)
  (:string 'font-lock-string-face)
  (:number 'font-lock-string-face)
  (:doc 'font-lock-doc-face))

;; Example:

(defun peg-arith-faces (str)
  (peg-parse-string
   ((expr _ sum)
    (sum product (* (or (and (peg-face "+" :operation)
                             _ product)
                        (and (peg-face "-" :operation)
                             _ product))))
    (product value (* (or (and (peg-face "*" :operation)
                               _ value)
                          (and (peg-face "/" :operation) _ value))))
    (value (or number
               (and "(" _ sum ")" _)))
    (number (peg-face (+ [0-9]) :number) _)
    (_ (* [" \t"]))
    (eol (or "\n" "\r\n" "\r")))
   str))

;; (peg-arith-faces "1 + 2 - 45 * (33 + 4)")

(provide 'peg-faces)
