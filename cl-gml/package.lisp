(defpackage #:gml
  (:use #:cl #:esrap)
  (:export #:parse-gml-string
           #:parse-gml-file
           #:->))
