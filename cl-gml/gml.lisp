;;;; cl-gml.lisp

(in-package #:gml)

(defun parse-gml-string (string)
  (parse 'gml string))

(defun parse-gml-file (pathname)
  (parse-gml-string (alexandria:read-file-into-string pathname)))

(defstruct gmlelem
  type id doc children options value args)

(defun alistp (alist)
  (and (listp alist)           
       (every #'consp alist)))

(defrule gml (and (* gml1) _))
(defrule gml1 (and _ elem)
  (:lambda (x) (second x)))

(defrule elem (and id-and-type _
                   (? (or elem-options elem-args)) _
                   (? elem-doc) _
                   (? elem-body) _
                   (? elem-options))
  (:destructure (id-and-type _ options-or-args _
                             doc _ body _ options)
                (let ((options (append (when (alistp options-or-args)
                                         options-or-args)
                                       options))
                      (type (cdr (assoc :type id-and-type)))
                      (id (cdr (assoc :id id-and-type)))
                      (args (when (not (alistp options-or-args))
                              options-or-args))
                      (value (and (eql (car body) :value)
                                  (cdr body)))
                      (children (and (eql (car body) :children)
                                     (cdr body))))
                  
                  ;; If type is nil and there is no doc, or args, or options
                  ;; just return the id as a string
                  (if (not (or type doc args options children value))
                      id
                      ;; else
                      (make-gmlelem
                       :type type
                       :id id
                       :doc doc
                       :args args 
                       :options options
                       :value value
                       :children children)))))

(defrule id-and-type (or id-before-type
                         type-before-id
                         id-only))
(defrule id-before-type (and id _ "::" _ id)
  (:lambda (x)
           (list (cons :id (first x))
                 (cons :type (nth 4 x)))))

(defrule type-before-id (and id _ id)
  (:lambda (x)
           (list (cons :type (first x))
                 (cons :id (third x)))))

(defrule id-only id
  (:lambda (x)
           (list (cons :type nil)
                 (cons :id x))))

(defrule idstartchar (character-ranges (#\a #\z) (#\A #\Z) #\_ #\% #\$ #\#))
(defrule idcontchar (or idstartchar (character-ranges (#\0 #\9) #\. #\-)))
(defrule id (and idstartchar (* idcontchar))
  (:text t)
  (:lambda (x)
           (make-symbol x)))

(defrule number (and (? #\-) (+ (character-ranges (#\0 #\9))))
  (:text t)
  (:lambda (x)
           (parse-integer x)))

(defrule boolean (or "true" "false")
  (:text t)
  (:lambda (x)
           (if (string= "true" x)
               t
               :false)))

(defrule string (or (and #\" (* (not #\")) #\")
                    (and #\' (* (not #\')) #\'))
  (:lambda (res)
           (text (second res))))

(defrule array (and #\[ _ (? array-items) _ #\])
  (:lambda (x)
           (third x)))

(defrule array-items1 (and array-item _ #\, _ array-items)
  (:lambda (x)
           (cons (first x) (nth 4 x))))
(defrule array-items2 array-item
  (:lambda (x)
           (list x)))

(defrule array-items (or array-items1 array-items2))

(defrule array-item (or array assoc boolean number string elem id))

(defrule elem-options (and #\[ _ (? elem-options-list) _ #\])
  (:lambda (x)
           (third x)))

(defrule elem-options-list1 (and elem-option _ #\, _ elem-options-list)
  (:lambda (x)
           (cons (first x) (nth 4 x))))

(defrule elem-options-list2 elem-option
  (:lambda (x) (list x)))

(defrule elem-options-list (or elem-options-list1
                               elem-options-list2))

(defrule elem-option (or assign-elem-option
                         false-elem-option
                         true-elem-option))

(defrule assign-elem-option (and id _ #\= _ optvalue)
  (:lambda (x)
           (cons (first x) (nth 4 x))))

(defrule false-elem-option (and #\! id)
  (:lambda (x)
           (cons (second x) :false)))

(defrule true-elem-option id
  (:lambda (x)
           (cons x t)))

(defrule elem-args (and #\( _ (? elem-arglist) _ #\) )
  (:lambda (x) (third x)))

(defrule elem-arglist (or (and propvalue _ #\, _ elem-arglist)
                          propvalue)
  (:lambda (x)
           (if (listp x)
               (cons (first x) (nth 4 x))
               (list x))))

(defrule propvalue (or array string number boolean lang elem id))
(defrule optvalue (or array string number boolean id))

;; Sublanguage support
;; lang rules are parsed with defined parsers if available.
;; If no parser is defined for a particular lang, then the string is returned.

(defvar *gml-langs* (make-hash-table :test 'equalp))

(defmacro define-lang (name parser)
  "Parser should be a function"
  (check-type parser (or symbol function))
  
  `(setf (gethash ,(string name) *gml-langs*)
         #',parser))

(defstruct gml-lang
  lang
  source
  ast)

(defrule lang (and "<<" id _ (* (not ">>")) ">>")
  (:lambda (x)
           (let ((lang (second x))
                 (source (text (nth 3 x))))
             (make-gml-lang
              :lang lang
              :source source
              :ast (anaphora:awhen (gethash (string (second x)) *gml-langs*)
                     (funcall anaphora:it source))))))

(define-lang rx ppcre:parse-string)

(defrule _ (* (or space line-comment)))
(defrule space (or #\space #\tab #\newline))

(defrule line-comment (and "//" (* (not #\newline)))
  (:lambda (x)
           (list :comment (text (second x)))))

(defrule elem-doc (and "<<" (* (not ">>")) ">>")
  (:lambda (x)
           (text (second x))))

(defrule assoc (and id #\= optvalue)
  (:lambda (x)
           (cons (first x) (third x))))

(defrule elem-body (or elem-children-body
                       elem-value-body))

(defrule elem-children-body (and #\{ _ children _ #\})
  (:lambda (x)
           (cons :children (third x))))

(defrule children (* child))
(defrule child (and elem _ #\; _)
  (:lambda (x) (first x)))

(defrule elem-value-body (and (or #\= #\:) _ propvalue)
  (:lambda (x)
           (cons :value (third x))))

(defun -> (elem path &key error (default nil defaultp))
  "GML element accessing function.

Examples:

(-> *user* '(&table :value))
(-> *user* '(:doc))
(-> *user* '(&attributes &name :val))
(-> *user* '(&attributes &name :type))

(-> *user* '(&attributes &email :opts unique) :default nil)
(-> *user* '(&attributes &email :opts edit) :default nil)
(-> *user* '(&attributes &password :opts serialize) :default t)
(-> *user* '(&attributes &name :opts serialize) :default t)

(-> *user* '(&db :opts softDelete) :default nil)
(-> *user* '(&attributes :children))"
  (cond
    ((null path) elem)
    ((gml::gmlelem-p elem)
     (let* ((key (first path))
            (subelem
             (case key
               ((:val :value) (gml::gmlelem-value elem))
               ((:opts :options) (gml::gmlelem-options elem))
               ((:args) (gml::gmlelem-args elem))
               ((:doc :documentation) (gml::gmlelem-doc elem))
               ((:id) (gml::gmlelem-id elem))
               ((:type) (gml::gmlelem-type elem))
               ((:children) (gml::gmlelem-children elem))
               (t (cond
                    ((symbolp key)
                     (if (char= (aref (symbol-name key) 0) #\&)
                         ;; Id
                         (or (find-if (lambda (child)
                                        (equalp (string (gml::gmlelem-id child))
                                                (subseq (symbol-name key) 1)))
                                      (gml::gmlelem-children elem))
                             (if defaultp
                                 (return-from -> default)
                                 (error "Child with id ~A not found in ~A"
                                        key elem)))
                         ;; Type
                         (or (find-if (lambda (child)
                                        (equalp (string (gml::gmlelem-type child))
                                                (symbol-name key)))
                                      (gml::gmlelem-children elem))
                             (if defaultp
                                 (return-from -> default)
                                 (error "Child with type ~A not found in ~A"
                                        key elem))))))))))
       (if (null subelem)
           (if (and (not (null (rest path))) defaultp)
               default
               (error "Cannot access to ~A in ~A" (rest path) elem))
           (-> subelem (rest path) :error error :default default))))
    ((listp elem)
     ;; Options?
     (cdr (assoc (string (first path))
                 elem
                 :key 'string
                 :test 'equalp)))))
