(asdf:defsystem #:gml
  :description "Generic Modeling Language parser"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (:esrap :alexandria :anaphora :ppcre)
  :components ((:file "package")
               (:file "gml")))
