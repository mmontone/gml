(require 'gml-mode)

(defun gml-test ()
  "(gml-test)"
  (interactive)
  (ert "gml") t)

(defmacro gml-parser-should (input output)
  `(should (equal (gml-parse-string ,input)
                  ,output)))

(ert-deftest gml-parser-name-and-type-test ()
  "GML parser tests"
  (gml-parser-should
   "Person Mariano"
   '(t
     (:elem
      ("Person" "Mariano")
      :opts
      ()
      :doc nil)))

  (gml-parser-should
   "Mariano"
   '(t
     (:elem
      ("Mariano" nil)
      :opts
      ()
      :doc nil)))

  (gml-parser-should
   "Mariano :: Person"
   '(t
     (:elem
      ("Person" "Mariano")
      :opts
      ()
      :doc nil)))
  )

(ert-deftest gml-parser-options-test ()
  "GML options test"
  (gml-parser-should
   "Foo [x=y]"
   '(t (:elem
        ("Foo" nil)
        :opts
        (("x" . "y"))
        :doc nil)))

  ;; Boolean options
  (gml-parser-should
   "Foo [opt]"
   '(t
     (:elem
      ("Foo" nil)
      :opts
      (("opt" . true))
      :doc nil)))

  (gml-parser-should
   "Foo [!opt]"
   '(t
     (:elem
      ("Foo" nil)
      :opts
      (("opt" . false))
      :doc nil)))
  )

(ert-deftest gml-parser-doc-test ()
  (let ((gml (second (gml-parse-string
                      "Mariano :: Person
                 << Elem doc >> "))))
    (should (equal (gml-doc gml)
                   "Elem doc")))

  (let ((gml (second (gml-parse-string
                      "Mariano :: Person
                 << Elem doc >> {} "))))
    (should (equal (gml-doc gml)
                   "Elem doc")))

  )

(ert-deftest gml-parser-values-test ()
  ;; Parse numbers
  (let* ((gml (second (gml-parse-string "Foo [x=22]"))))
    (should (numberp (gml-opt "x" gml)))
    (should (equal (gml-opt "x" gml) 22)))

  ;; Parse identifiers as strings
  (let* ((gml (second (gml-parse-string "Foo [x=bar]"))))
    (should (stringp (gml-opt "x" gml)))
    (should (equal (gml-opt "x" gml) "bar")))

  ;; Parse strings
  (let* ((gml (second (gml-parse-string "Foo [x=\"bar\"]"))))
    (should (stringp (gml-opt "x"gml)))
    (should (equal (gml-opt "x" gml) "bar")))

  ;; Arrays
  ;; (let* ((gml (second (gml-parse-string "Foo [x=[foo,bar]"))))
  ;;   (should (listp (gml-opt "x" gml)))
  ;;   (should (equal (gml-opt "x" gml) '("foo" "bar"))))

  )

(ert-deftest gml-parser-access-form-test ()
  (gml-parser-should
   "Person->name(x)"
   '(t
     (:elem
      ("Person" nil)
      :opts nil :doc nil :access "name" :args
      ((:elem
        ("x" nil)
        :opts nil :doc nil))))))

(ert-deftest gml-parser-args-form-test ()
  (let ((gml (second (peg-parser-parse-string
                      'gml
                      "hello(22, 44)"))))
    (should (equal (gml-args gml) (list 22 44))))

  (peg-parser-parse-string
   'gml
   "foo(bar(baz(), 33))")
  )

(ert-deftest gml-parser-value-form-test ()
  (let ((gml (second
              (peg-parser-parse-string
               'gml
               "hello = 22"))))
    (should (equal (gml-value gml) 22)))

  (let ((gml (second
              (peg-parser-parse-string
               'gml
               "hello:22"))))
    (should (equal (gml-value gml) 22)))
  )

(ert-deftest gml-parser-children-test ()
  (gml-parser-should
   "Person Mariano [required]{
    foo: 'lalla';
bar: 22;
validate: true;
}"
   '(t
     (:elem
      ("Person" "Mariano")
      :opts
      (("required" . true))
      :doc nil :children
      ((:elem
        ("validate" nil)
        :opts nil :doc nil :value true)
       (:elem
        ("bar" nil)
        :opts nil :doc nil :value 22)
       (:elem
        ("foo" nil)
        :opts nil :doc nil :value "lalla"))))))

(ert-deftest gml-parser-comments-test ()
  (gml-parser-should
   "Person Mariano {
   // This is a comment
   bar: [];
}"
   '(t
     (:elem
      ("Person" "Mariano")
      :opts nil :doc nil :children
      ((:elem
        ("bar" nil)
        :opts nil :doc nil :value nil))))))

(peg-parser-parse-string
 'gml
 "Mariano :: Mold.Person [optional]
<< This is Mariano >>
{
   siblings: [Martin, Marcos];
}")

(peg-parser-parse-string
 'gml
 "Person Mariano [a]{}")

(peg-parser-parse-string
 'gml
 "Person Mariano {}[a]")

(peg-parser-parse-string
 'gml
 "Person Mariano []{}")

(peg-parser-parse-string
 'gml
 "Person [!req]{}")

(peg-parser-parse-string 'gml "lala")

(peg-parser-parse-string
 'gml
 "Person Mariano {
    foo: 22;
bar: lala;
bar: Martin [age=33];
}")



(peg-parser-parse-string
 'gml
 "Person Mariano {
    sisters { x : 22; };
    brothers{};

}")

(peg-parser-parse-string
 'gml
 "Mariano {
    brothers {
     foo:var;
};
    sisters: [];
}")

(peg-parser-parse-string
 'gml
 "Form UserForm [method=POST]
  <<The form for entering user info>>
  {
     fields {
             String name [required]
             <<The name should be a real person name>>;

             String lastname [required];
    };
}[clientValidation=true]")

(peg-parser-parse-string
 'gml
 "form ValidatedForm [action='/validation-post',
                      !clientValidation]
<< A validated form!! >>
{
    String name [value='',
                 constraints=[string,notBlank,max=5]];
    Choice sex [choices=[Male,Female],
                value=Male];
    Integer age [constraints=[integer,greaterThan(-1), lessThan(200)]];
    Email email <<The email>>;
    Date birthDate [!required]
    <<The birthdate is a date>>;

    Submit submit [label=Create];
}")

(peg-parser-parse-string
 'gml
 "schema user
  << The user schema >>
  {
    id :: Integer;
    realname :: String;
}")
