(require 'peg-faces)

(defun gml-parse-buf ()
  (peg-parser-parse-string
   'gml-faces
   (buffer-substring-no-properties 1 (buffer-size))))

(defun gml-highlight ()
  (interactive)
  (let ((regions
         (rest
          (peg-parser-parse-string
           'gml-faces
           (buffer-substring-no-properties 1 (buffer-size))))))
    (dolist (region regions)
      (set-text-properties (second region)
                           (third region)
                           (list 'face (funcall 'gml-faces-table (first region)))))))

(def-peg-faces-table gml-faces-table
  (:var 'font-lock-variable-name-face)
  (:builtin 'font-lock-builtin-face)
  (:keyword 'font-lock-keyword-face)
  (:type 'font-lock-type-face)
  (:constant 'font-lock-constant-face)
  (:comment 'font-lock-comment-face)
  (:string 'font-lock-string-face)
  (:number 'font-lock-string-face)
  (:doc 'font-lock-doc-face)
  (:true '(:foreground "green"))
  (:false '(:foreground  "red")))

(peg-def-parser
 gml
 (gml (* _ elem))
 (elemids (or
           (and identifier _ "::" _ identifier
                `(id type -- (list type id)))
           (and identifier _ identifier `(type id -- (list type id)))
           (and identifier `(type -- (list type nil)))))
 (elems (and `(-- nil)
             (*
              elem _ ";" _
              `(elems elem -- (cons elem elems)))))
 (elem-value-body (or "=" ":") _
                  propvalue
                  `(val -- (list :value val)))
 (elem-access-body "->"
                   (or (and identifier _
                            "(" _ (or elemargs* `(-- nil)) _ ")"
                            `(name args -- (list :access name :args args)))
                       (and identifier `(name -- (list :access name)))))
 (elem-children-body
  (and "{" _
       `(-- nil)
       (* elem _ ";"_ `(elems elem -- (cons elem elems)))
       "}"
       `(children -- (list :children children))))
 (elem-body (or elem-children-body
                elem-value-body
                elem-access-body
                ;;elem-apply-body
                ))
 (elem (and elemids  _
            (or elemargs elemoptions `(-- nil))  _
            (or (and elemdoc
                     `(doc -- (string-trim doc)))
                `(-- nil)) _
                (or elem-body `(-- nil)) _
                (or elemoptions `(-- nil))
                `(ids opts doc body opts2 --
                      (append
                       (list :elem ids
                             :opts (append opts opts2)
                             :doc doc)
                       body))))
 (elemargs* (or (and propvalue _ "," _ elemargs*
                     `(arg args -- (cons arg args)))
                (and propvalue `(arg -- (list arg)))))
 (elemargs "(" _ (or elemargs* `(-- nil)) _ ")"
           `(args -- (list :args args)))
 (elemoptions (and "[" (or elemoptions1 `(-- nil)) "]"))
 (elemoptions1 (or (and elemoption _ "," _ elemoptions1
                        `(opt opts -- (cons opt opts)))
                   (and elemoption `(opt -- (list opt)))))
 (elemoption (or (and identifier
                      _ "=" _
                      optvalue
                      `(id val -- (cons id val)))
                 (and "!" identifier `(id -- (cons id 'false)))
                 (and identifier `(id -- (cons id 'true)))))
 (optvalue (or array boolean number string identifier))
 (propvalue (or array boolean number string lang elem identifier))
 (identifier (substring (+ identchar)))
 (identchar [a-z A-Z 0-9 "._%$#/"])
 (string (or (and "'" (substring  (* (not "'") (any))) "'")
             (and "\"" (substring  (* (not "\"") (any))) "\"")))
 ;; (escape-string (and (and "<" escape-char)
 ;;                     (substring (* (not (and escape-char ">"))))
 ;;                     (and escape-char ">")))
 (number (substring (+ [0-9])) `(s -- (string-to-number s)))
 (boolean (and (substring (or "true" "false")) `(s -- (intern s))))
 (array (and "[" _ (or array-items `(-- nil)) _ "]"))
 (array-items (or (and array-item _ "," _ array-items
                        `(value values -- (cons value values)))
                   (and array-item `(val -- (list val)))))
 (assoc (and optvalue "=" optvalue
             `(key val -- (cons key val))))
 (array-item (or array assoc boolean number string elem identifier))
 (elemdoc (and "<<" (substring (* (not ">>") (any))) ">>"))
 (lang (and "<<" identifier _ (substring (* (not ">>") (any))) ">>"
            `(lang source -- (list :lang lang source))))
 (multiline-comment (and "<<" (* (not ">>") (any)) ">>"))
 (line-comment "//" (* (not (or "\n" (eob))) (any)))
 (_ (* (or [" \t\n"] line-comment)))
 (eol (or "\n" "\r\n" "\r")))

(peg-def-parser
 gml-faces
 (gml (* _ elem))
 (elemids (or
           (and (peg-face prim-identifier :var)
                _ (peg-face "::" :builtin) _
                (peg-face prim-identifier :type))
           (and (peg-face prim-identifier :type)
                _
                (peg-face prim-identifier :var))
           identifier))
 (elems (*
         elem _ (peg-face ";" :keyword) _))
 (elem-args (or (and propvalue _ (peg-face "," :keyword) _ elem-args)
                propvalue))
 (elem-apply-body (peg-face "(" :keyword) _
                  (opt elem-args) _
                  (peg-face ")" :keyword))
 (elem-value-body (peg-face (or "=" ":") :keyword)  _
                  propvalue)
 (elem-children-body
  (and (peg-face "{" :keyword) _
       (*
        elem _ (peg-face ";" :keyword) _)
       (peg-face "}" :keyword)))
 (elem-body (or elem-children-body
                elem-value-body
                ;;elem-apply-body
                ))
 (elem (and elemids  _
            (opt elemoptions) _
            (opt elemdoc) _
            (opt elem-body) _
            (opt elemoptions)))
 (elemoptions (or
               (and (peg-face "[" :keyword)
                    (opt elemoptions1)
                    (peg-face "]" :keyword))
               elem-apply-body))
 (elemoptions1 (or (and elemoption _ (peg-face "," :keyword) _ elemoptions1)
                   elemoption))
 (elemoption (or (and identifier
                      _ (peg-face "=" :keyword) _
                      optvalue)
                 (peg-face (and "!" prim-identifier) :false)
                 (peg-face prim-identifier :true)))
 (optvalue (or array boolean number string identifier))
 (propvalue (or array boolean number string lang elem identifier))
 (identifier (peg-face (+ identchar) :var))
 (prim-identifier (+ identchar))
 (typename (peg-face prim-identifier :type))
 (identchar [a-z A-Z 0-9 "-._%$#/"])
 (string (peg-face
          (or (and "'" (* (not "'") (any)) "'")
              (and "\"" (* (not "\"") (any)) "\""))
          :string))
 (number (peg-face (+ [0-9]) :number))
 (boolean (or (peg-face "true" :true)
              (peg-face "false" :false)))
 (array (and (peg-face "[" :keyword) _ (opt array-items) _
             (peg-face "]" :keyword)))
 (array-items (or (and array-item _ (peg-face "," :keyword) _ array-items)
                   array-item))
 (assoc (and optvalue (peg-face "=" :keyword) optvalue))
 (array-item (or array assoc boolean number string elem identifier))
 (elemdoc (peg-face (and "<<" (* (not ">>") (any)) ">>") :doc))
 (lang (peg-face (and "<<" prim-identifier _ (* (not ">>") (any)) ">>") :string))
 (line-comment (peg-face (and "//" (* (not (or "\n" (eob))) (any))) :comment))
 (_ (* (or [" \t\n"] line-comment)))
 (eol (or "\n" "\r\n" "\r")))

(define-derived-mode gml-mode java-mode ;;fundamental-mode  prog-mode
  "GML mode"
  "GML mode is a major mode for editing GML files"

  ;;(set (make-local-variable 'syntax-propertize-function)
  ;;     'gml-syntax-propertize-function)

  ;;(set (make-local-variable 'font-lock-unfontify-region-function) nil)

  (set (make-local-variable 'peg-faces) 'gml-faces)
  (set (make-local-variable 'peg-faces-table) 'gml-faces-table)

  (set (make-local-variable 'font-lock-fontify-region-function)
       #'peg-faces-propertize)

  ;; you again used quote when you had '((gml-hilite))
  ;; I just updated the variable to have the proper nesting (as noted above)
  ;; and use the value directly here
  ;;(setq font-lock-defaults gml-font-lock-defaults)
  ;;(set (make-local-variable 'font-lock-defaults) '(()))

  ;; when there's an override, use it
  ;; otherwise it gets the default value
  ;;(when gml-tab-width
  ;;  (setq tab-width gml-tab-width))

  ;; Note that there's no need to manually call `gml-mode-hook'; `define-derived-mode'
  ;; will define `gml-mode' to call it properly right before it exits
  )

(defun gml-parse-string (string)
  (peg-parser-parse-string 'gml string))

;; Accessing

(defun gml-is-elem? (gml)
  (equal (first gml) :elem))

(defun gml-id (gml)
  (assert (gml-is-elem? gml))
  (second (second gml)))

(defun gml-type (gml)
  (assert (gml-is-elem? gml))
  (first (second gml)))

(defun gml-opts (gml)
  (assert (gml-is-elem? gml))
  (getf (cddr gml) :opts))

(defun gml-opt (opt gml)
  (cdr (assoc opt (gml-opts gml))))

(defun gml-doc (gml)
  (assert (gml-is-elem? gml))
  (getf (cddr gml) :doc))

(defun gml-value (gml)
  (assert (gml-is-elem? gml))
  (getf (cddr gml) :value))

(defun gml-args (gml)
  (getf (gml-opts gml) :args))

(provide 'gml-mode)
