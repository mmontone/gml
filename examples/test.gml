form {
    // comment
    div [class=form-group] {
        label [for=email] = "Email address:";
        Input email [type=email];
    };

    div [class=form-group] {
        label[for=pwd] = "Password";
        Input pwd [type=password];
    };

    button[class="btn btn-info"] : "Submit";
}
