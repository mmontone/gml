Clon.Synopsis EruditeSynopsis [postfix="FILES..."]
    << Erudite is a Literate Programming System >>
{
    Flag [short="h",long="Help"]
        <<Print this help and exit>>;
    Flag [long="version"]
        <<Print Erudite version>>;
    Switch [short="v",long="verbose",default=false]
        <<Run in verbose mode>>;
    Switch [short="d",long="debug",argumentStyle="on/off",
              default=false,envVar="DEBUG"]
        <<Turn debugging on or off>>;
    Enum [long="syntax", argName="SYNTAX",
                 enum=[erudite, latex, sphinx, markdown, org],
                 default=erudite]
        <<The syntax used in source files>>;
}
