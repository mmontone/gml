GladeInterface [requires=gnome] {

  GtkDialog MainWindow {
    props {
      border_width: 6;
      visible: true;
      title [translatable] = "Useless Program";
      type: GKT_WINDOW_TOPLEVEL;
      window_position: none;
      modal: false;
      destroy_with_parent: false;
      has_separator: false;
      Signal delete_event [handler=gtk_main_quit];
    };

    VBox dialog-vbox {
      props {
        visible: true;
        homogeneous: false;
        spacing: 12;
      };

      GtkHButtonBox dialog-action_area {
        props {
          visible: true;
          layout_style: buttonbox_end;
        };

        GtkButton OKButton {
          props {
            visible: true;
            can_default: true;
            can_focus: true;
            use_stock: true;
            label: gtk-ok;
          };
          Signal clicked [handler=ok_button_clicked];
        };

        packing {
          padding: 0;
          expand: false;
          fill: true;
          pack_type: end;
        };
      };
    };
  };
}
