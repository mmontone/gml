RestAPI api-test [title="Api test"]
    << A test api >>
{

    Resource parameters [produces=[JSON], path="/parameters"]
        << Parameters resource >>
    {
        Route parameters [path="/parameters"]
            << Parameters test >>
        {
            args {x; y; z; };
            
            optional {
                Boolean boolean;
                Integer integer;
                String string;
                List list;
            };
        };
    };

    Resource users [produces=[JSON, XML],
                    consumes=[JSON],
                    models=[User],
                    path="/users"]

        << Users resource >>

    {
        Route getUsers [method=GET, path="/users"]
            << Retrieve the users list >>
        {
            optional {
                // Translates to <Integer id="page" value="1">
                page :: Integer = 1;
                page :: Integer : 1;
                page :: Integer [value=1];
                expand :: List;
            };
        };

        Route getUser [method=GET, path="/users/{id}"]
            << Retrieve an user >>
        {
            id :: Integer <<The user id>>;
            expand :: List [optional];
        };


        // Alternative syntax?
        // Translates to <getUser2 arg1="GET" arg2="/users/{id}">
        //getUser2 [GET, "/users/{id}"]
        Route getUser2 (GET, "/users/{id}")
            << Retrieve an user >>
        {
            id :: Integer; // the user id
            expand :: List [optional]; // short for optional=True
        };
    };
}


Schema User {
    id :: Integer;
    realname :: String;
}
