Form MemberForm {
    name :: String [default="", !required]; // required=false
    ready :: Boolean [default=false, !required];
    sex :: Choice [default=Male, choices=[Male, Female]];
}

Class Form
     << A form >>
{
    id :: Symbol [initarg=id,accessor=form-id];
    name :: String [required, initarg=name];
    action :: String <<The action>>;
    method :: Symbol;
}

Form CompositionForm [action="/composition-post",
                      method="POST"]
{
    mainMember :: Subform [subform=MemberForm];
    todo :: List [type=String];    
}


Form ValidatedForm [action="/validation-post", !clientValidation]
   << adfadfsdf >>
{
    String name [value="",
                 constraints=[string,notBlank,max(5)]];
    Choice sex [choices=[Male,Female],
                value=Male];
    Integer age [constraints=[integer,
                              greaterThan(-1),
                              lessThan(200, msg="Should be less than 200")]];
    Email email;
    Date birthDate [!required];
    Submit submit [label=Create];
}

Form AnotherForm [action="/validation-post", clientValidation]
{
    String name [default="",
                 constraints=[string,notBlank,max(5)]];
    Choice sex [choices=[Male,Female],
                default=Male];
    Integer age [constraints=[integer,greaterThan(-1), lessThan(200)]];
    Email email;
    Date birthDate [!required];
    Submit submit [label=Create];
}
