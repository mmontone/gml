Form MyForm [method=POST, action="/mypost"]
    << My form documentation >>
{
    fields {
        String name [required];
        String lastname [required];

        // Alternative syntax?:
        age::Number [optional];

        siblings :: List[type=Form];
    };
}

// <Form id="MyForm" method="POST">
//       <documentation>My form documentation</documentation>
//       <fields>
//         <string id="name" required="true"/>
//         <string id="lastname" required="true"/>
//       </fields>
// </Form>


Person Mariano
    << Mariano is the older brother >>
{
    age: 35;

    // Arrays?:

    siblings: [Martin, Marcos, Milagros];
    Parents {
        Person Rafa [age=64] <<Rafael>>;
        Person Ana [age=65];
    };
    description: "Blah blah blah.
       Blah blah blah.";
}

Person Martin {

}
[age=33]

Config MyConfig
    
    << My configuration >>
{
    Section Host
        << The web server configuration >>
    {
        ServerName: "localhost";
        ServerPort: 34;
    };

    Section DB
        << The database configuration >>
    {
        Name: "MyDB";
    };
}

DB.Table User [tableName="users"]

{
    id :: Autoincrement [primaryKey];
    password :: Password;
    friends :: FriendTable [foreignKey=id];
}

DB.Table Friend [tableName="friends"] {

    fields {
        id :: Autoincrement [primaryKey];
        friend1 :: Integer [foreignKey=User.id];
        friend2 :: Integer [foreignKey=User.id];
    };

    constraints {
        not_equal_friend: notEqual(friend1, friend2);
    };
}

Class MyClass [extends=MySuperClass]
    << This is my class >>
{
    name :: String [required, accessor,
                    initarg=name,
                    reader, writer,
                    reader=getName, writer=setName]
        <<The user name>>;
    sex :: Enum [values=[Male,Female]];
    //age :: Integer [required, validator=between(1, 100)];
}
[metaClass=StandardMetaClass]
