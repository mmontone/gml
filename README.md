# GML

GML stands for Generic Modeling Language. It is a human-readable language for describing models.

## Quick example

```gml
Model Person 
   <<A model for a person >>
{   
    String fullname [required] <<the person name and lastname>>;
    age :: Integer [required] <<the person age>>;
    sex :: Enum [options=[Male, Female]];
    email :: Email;
}

Person {
    fullname = "Mariano Montone";
    age = 35;
    sex = Male;
    email = "marianomontone@gmail.com";
}
```

## Overview

### Elements

The basic GML element has the form:

```
ElementTypeAndId [opt1=val1, opt2=val2]
    <<Element documentation>>
{
    ... children ...
}
[opt3=val3]
```

where:

#### Element type and id 
Elements have a type and identifier. ElementTypeAndId can have the form:

   * `ElementId` just the element identifier.
   * `ElementType ElementId` the element type followed by the element id.
   * `ElementId::ElementType` the element id followed by the element type.
  
  For example, these are valid elements types and ids:
  
  * `Person Peter`
  * `Person`
  * `Peter::Person`
  
#### Element options  
Elements can have options. They are specified between brackets and are optional. They have the form: `[id1=val1, id2=val2, trueOption, !falseOption]`.

Values can be `string`, `number`, `boolean`, `array`, `identifier` (see primitive types).
  
  If no equal operator is used, then the option is parsed as a boolean option.
  If the option starts with `!`, then the option is set to `false`. Otherwise, the option is set to `true`.  For example:
  
  * `[required]` is equivalent to `[required=true]`.
  * `[!required]` is equivalent to: `[required=false]`

#### Element children
Element children are elements too, and **must finish in ;** (for the current version of the implementation at least).
Like: 
```gml
MyElement {
   foo;
   bar = 2;
   baz {};
}
```
Again, the ending `;` is mandatory.

#### Element comments

Contrary to `line comments`, element comments are attached to their corresponding element. They are specified between `<<` and `>>`, before the element body.
Examples:
```gml
MyElement 
   <<this a cool element>> 
{
   foo <<this is foo comment>> = 34;
   bar <<this is bar>> (45);
}
```

#### Value elements

Elements can have the form of value assignments. Value elements starts like normal elements, but instead of `{children}` they are `= value`.

So, for example, this is a valid value element:
```gml
Integer age = 22
```
That is an element with type Integer, id `age` and value 22.
They commonly appear as child elements.

#### Apply elements

Apply elements are elements that represent application, like in `function application`. As value elements, they differ in the element body. Instead of `{children}` or `= val`, they are `(arg1, ..., argN)`.

So, for example, this is a valid apply element:
```gml
greaterThan(33)
```
Is an element named `greaterThan` with arguments `33`.

Apply elements are useful for math, code, etc.


### Primitive types

#### Identifiers
Alphanumeric without spaces are parsed as identifiers or symbols:
`foo` `MyId` `bar123`.
#### String
Strings are specified between double quotes:`"my string"`
#### Number
GML supports simple integers: `1234` `-1234`
#### Boolean
Booleans are `true` and `false`
#### Arrays
Arrays have this syntax: `[item1,item2]` where item can be either a GML element, or a primitive type.
#### Associations
Associations are the relation between two things: `key->value`.
They are useful together with arrays: `[key1->value1, key2->value2]`.  

### Line comments

Line comments start with: `//`. Everything after that until the end of the line is ignored.

## XML equivalences


GML can be directly translated to XML, back and forth:

<table border="0">
 <tr>
    <td>
        <b style="font-size:30px">GML</b>
    </td>
    <td>
        <b style="font-size:30px">XML</b>
    </td>
 </tr>
 <tr>
    <td><code>MyElement</code></td>
    <td><code>&lt;element id="MyElement"&gt;</code></td>
 </tr>
  <tr>
    <td><code>ElementType MyElement</code></td>
    <td><code>&lt;ElementType id="MyElement"&gt;</code></td>
 </tr>
  <tr>
    <td><code>MyElement::ElementType</code></td>
    <td><code>&lt;ElementType id="MyElement"&gt;</code></td>
 </tr>
 <tr>
    <td><code>age = 22</code></td>
    <td><code>&lt;element id="age" value="22"&gt;</code></td>
 </tr>
</table>

## Repository contents

This is what there's in this repository:

* `peg-mode.el`: An Emacs major mode for editing PEG grammar files.
* `peg-faces.el`: An Emacs package that applies faces to a buffer using a PEG grammar.
* `epeg.peg`: An extended version of PEGs that supports attributes, actions, and more.
* `gml-mode.el`: Emacs mode for editing GML (syntax highlighting).
* `gml-test.el`: Tests for Emacs GML implementation.
* `GML.pck.st`: A Cuis Smalltalk implementation of GML.
* `cl-gml`: Common Lisp implementation of GML.
* `examples`: Folder with several GML examples.

## Examples

### A web form

```gml
Form MemberForm {
    name :: String [default="", !required]; // required=false
    ready :: Boolean [default=false, !required];
    sex :: Choice [default=Male, choices=[Male, Female]];
}

Class Form
     << A form >>
{
    id :: Symbol [initarg=id,accessor=form-id];
    name :: String [required, initarg=name];
    action :: String <<The action>>;
    method :: Symbol;
}

Form CompositionForm [action="/composition-post",
                      method="POST"]
{
    mainMember :: Subform [subform=MemberForm];
    todo :: List [type=String];
}


Form ValidatedForm [action="/validation-post", !clientValidation]
   << A validated web form >>
{
    String name [value="",
                 constraints=[string,notBlank,max(5)]];
    Choice sex [choices=[Male,Female],
                value=Male];
    Integer age [constraints=[integer,
                              greaterThan(-1),
                              lessThan(200, msg="Should be less than 200")]];
    Email email;
    Date birthDate [!required];
    Submit submit [label=Create];
}

Form AnotherForm [action="/validation-post", clientValidation]
{
    String name [default="",
                 constraints=[string,notBlank,max(5)]];
    Choice sex [choices=[Male,Female],
                default=Male];
    Integer age [constraints=[integer,greaterThan(-1), lessThan(200)]];
    Email email;
    Date birthDate [!required];
    Submit submit [label=Create];
}

```

## A GTK view (Glade XML equivalent)

```gml
GladeInterface [requires=gnome] {

  GtkDialog MainWindow {
    props {
      border_width: 6;
      visible: true;
      title [translatable] = "Useless Program";
      type: GKT_WINDOW_TOPLEVEL;
      window_position: none;
      modal: false;
      destroy_with_parent: false;
      has_separator: false;
      Signal delete_event [handler=gtk_main_quit];
    };

    VBox dialog-vbox {
      props {
        visible: true;
        homogeneous: false;
        spacing: 12;
      };

      GtkHButtonBox dialog-action_area {
        props {
          visible: true;
          layout_style: buttonbox_end;
        };

        GtkButton OKButton {
          props {
            visible: true;
            can_default: true;
            can_focus: true;
            use_stock: true;
            label: gtk-ok;
          };
          Signal clicked [handler=ok_button_clicked];
        };

        packing {
          padding: 0;
          expand: false;
          fill: true;
          pack_type: end;
        };
      };
    };
  };
}
```
## PEG grammar

```gml
gml <- (_ elem)*

elem <- id_and_type _
        (elem_options / elem_args)? _
        elemdoc? _
        elem_body? _
        elem_options?

idchar <- [a-zA-Z0-9._%$#]
id     <- idchar+

id_before_type <- id _ '::' _ id
type_before_id <- id _ id
id_only     <- id
id_and_type <- id_before_type /
               type_before_id /
               id_only

elem_args <- '(' _ elem_arglist? _ ')'
elem_arglist <- propvalue _ (',' propvalue)*

elem_value_body <- ('=' / ':') _ propvalue
elem_children_body <- '{' _ children _ '} '
children <- (elem _ ';' _)* 
elem_body <- elem_children_body / elem_value_body

elem_options <- '[' _ elem_options_list? _ ']'
elem_options_list <-  elemoption (_ ',' _ elemoption)*

elemoption <-  assign_elemoption /
               false_elemoption /
               true_elemoption 

assign_elemoption <- id _ '=' _ optvalue
true_elemoption <- id
false_elemoption <- '!' id

optvalue <- assoc / array / boolean / number / string / id
propvalue <- array / boolean / number / string / elem / id

string <- ['] (!['] Char)* ['] /
          ["] (!["] Char)* ["]

boolean <- 'true' / 'false'

number <- [0-9]+

array <- '[' _ array_items? _ ']'
array_items <- array_item _ ',' _ array_items / array_item
array_item <- array / assoc / boolean / number / string / elem / id

assoc <- id '=' optvalue

elemdoc <- '<<' (!'>>' Char)* '>>'

line_comment <- '//' (!EndOfLine .)* (EndOfLine / EndOfFile)

Char        <- '\\' [nrt'"\[\]\\]
            /  '\\' 'u' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' 'U' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' [0-3] [0-7] [0-7]
            /  '\\' [0-7] [0-7]?
            /  !'\\' .

_           <- (Space / line_comment)*

Space       <- ' ' / '\t' / EndOfLine
EndOfLine   <- '\r\n' / '\n' / '\r'
EndOfFile   <- !.peg
```
